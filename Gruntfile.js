module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				files: {
					'css/styles.css' : 'scss/styles.scss'
				}
			}
		},
		uglify: {
			my_target: {
				files: {
					'js/app.min.js': ['js/app.js']
				}
			}
		}
	});
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.registerTask('default', ['sass', 'uglify']);
}