(function() {
	google.load("feeds", "1");
	google.setOnLoadCallback(initialize);
	var ready = false;
	function initialize() {
		ready = true;
	}

	var app = angular.module("homemade", []);

	app.controller("PageCtrl", ["$scope", function($scope) {

		$scope.categories = [
			{name: "Global and UK", feeds: [
				{name: "Africa", link: 'http://feeds.bbci.co.uk/news/world/africa/rss.xml'},
				{name: "Asia", link: 'http://feeds.bbci.co.uk/news/world/asia/rss.xml'},
				{name: "Europe", link: 'http://feeds.bbci.co.uk/news/world/europe/rss.xml'},
				{name: "Latin America", link: 'http://feeds.bbci.co.uk/news/world/latin_america/rss.xml'},
				{name: "Middle East", link: 'http://feeds.bbci.co.uk/news/world/middle_east/rss.xml'},
				{name: "US & Canada", link: 'http://feeds.bbci.co.uk/news/world/us_and_canada/rss.xml'},
				{name: "England", link: 'http://feeds.bbci.co.uk/news/england/rss.xml'},
				{name: "Northern Ireland", link: 'http://feeds.bbci.co.uk/news/northern_ireland/rss.xml'},
				{name: "Scotland", link: 'Scotland'},
				{name: "Wales", link: 'http://feeds.bbci.co.uk/news/wales/rss.xml'}
			]},
			{name: "Video and Audio", feeds: [
				{name: "Top Stories", link: 'http://feeds.bbci.co.uk/news/rss.xml'},
				{name: "World", link: 'http://feeds.bbci.co.uk/news/world/rss.xml'},
				{name: "UK", link: 'http://feeds.bbci.co.uk/news/uk/rss.xml'},
				{name: "Business", link: 'http://feeds.bbci.co.uk/news/business/rss.xml'},
				{name: "Politics", link: 'http://feeds.bbci.co.uk/news/politics/rss.xml'},
				{name: "Health", link: 'http://feeds.bbci.co.uk/news/health/rss.xml'},
				{name: "Science & Environment", link: 'http://feeds.bbci.co.uk/news/science_and_environment/rss.xml'},
				{name: "Technology", link: 'http://feeds.bbci.co.uk/news/technology/rss.xml'},
				{name: "Entertainment & Arts", link: 'http://feeds.bbci.co.uk/news/entertainment_and_arts/rss.xml'}
			]},
			{name: "Other", feeds: [
				{name: "Latest published stories", link: 'http://feeds.bbci.co.uk/news/system/latest_published_content/rss.xml'},
				{name: "Magazine", link: 'http://feeds.bbci.co.uk/news/magazine/rss.xml'},
				{name: "Also in the news", link: 'http://feeds.bbci.co.uk/news/also_in_the_news/rss.xml'},
				{name: "In Pictures", link: 'http://newsrss.bbc.co.uk/rss/newsonline_uk_edition/in_pictures/rss.xml'},
				{name: "Special Reports", link: 'http://feeds.bbci.co.uk/news/special_reports/rss.xml'},
				{name: "Have your say", link: 'http://feeds.bbci.co.uk/news/have_your_say/rss.xml'},
				{name: "Editors Blog", link: 'http://www.bbc.co.uk/blogs/theeditors/rss.xml'},
				{name: "BBC Sport feeds", link: 'http://news.bbc.co.uk/sport1/hi/help/rss/default.stm'}
			]}
		];	
		$scope.page = 1;
		$scope.pages = 0;
		$scope.pageSize = 5;
		$scope.category = null;
		$scope.feed = null;
		$scope.sort = "publishedDate";
		$scope.items = [];

		this.fetchFeed = function() {
			if ($scope.feed == null) {
				return;
			}
			if (!ready) {
				alert('uh oh, not ready!');
				return;
			}
			var feed = new google.feeds.Feed($scope.feed.link);
			feed.setNumEntries(40);
			feed.load(function(result) {
				if (!result.error) {
					$scope.items = result.feed.entries;
					$scope.feed.description = result.feed.description;
					$scope.feed.title =  result.feed.title;
					$page = 1;
					$scope.pages = Math.ceil($scope.items.length/$scope.pageSize);
					$scope.$apply();
				}
			});
		};
		this.changePageSize = function() {
			$page = 1;
			$scope.pages = Math.ceil($scope.items.length/$scope.pageSize);
		};
		var self = this;
		$scope.$watch('feed', function() {self.fetchFeed();});
		$scope.$watch('pageSize', function() {self.changePageSize();});

		this.openLink = function(link) {
			window.open(link)
		}

	}]);

	app.filter('startFrom', function() {
	    return function(input, start) {
	        start = +start;
	        return input.slice(start);
	    }
	});


})();